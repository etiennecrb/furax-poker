import setuptools

from furax_poker import __version__

with open("README.rst", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="furax-poker",
    version=__version__,
    author="Étienne Corbillé",
    author_email="etienne.corbille@protonmail.com",
    description="A simple poker HUD",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.com/etiennecrb/furax-poker",
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=[
        "alembic",
        "appdirs",
        "PyQt5",
        "requests",
        "sqlalchemy",
        "watchdog",
    ],
    extras_require={
        "dev": [
            "flake8",
            "pytest",
            "pre-commit",
            "black",
            "mypy",
            "sqlalchemy-stubs",
            "sphinx",
            "pyinstaller",
        ]
    },
    entry_points={"console_scripts": ["furax = furax_poker.gui.main:main"]},
)
