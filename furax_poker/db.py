from contextlib import contextmanager, nullcontext
import logging
import os
import tempfile
import threading

from alembic import command
from alembic.config import Config
from appdirs import user_data_dir
from sqlalchemy import Column, create_engine, inspect, Integer, event
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import sessionmaker, Session as Session_

from furax_poker.utils import resource_path

logger = logging.getLogger(__name__)


class DeclarativeBase:
    """Custom base class to share some common properties among models.

    In particular, it registers SQLAlchemy event listeners to log database insert,
    update and delete operations.
    """

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = Column(Integer, primary_key=True)

    def __repr__(self):
        return f"{self.__class__.__name__}(id={self.id})"


@event.listens_for(DeclarativeBase, "after_insert", propagate=True)
def _log_after_insert(mapper, connection, target):
    logger.debug(f"Added: {target}")


@event.listens_for(DeclarativeBase, "after_delete", propagate=True)
def _log_after_delete(mapper, connection, target):
    logger.debug(f"Deleted: {target}")


@event.listens_for(DeclarativeBase, "after_update", propagate=True)
def _log_after_update(mapper, connection, target):
    state = inspect(target)
    current = {}
    previous = {}

    # Track target changes
    for attr in state.attrs:
        hist = state.get_history(attr.key, True)
        if hist.has_changes():
            new_value = hist.added[0] if hist.added else None
            old_value = hist.deleted[0] if hist.deleted else None
            if new_value != old_value:
                current[attr.key] = new_value
                previous[attr.key] = old_value

    if current:
        logger.debug(f"Updated: {target!r} with {current}, previously {previous}")


Session = sessionmaker()
Base = declarative_base(cls=DeclarativeBase)
writing_session_lock = threading.RLock()


@contextmanager
def session_scope(write=False):
    """Provide a transactional scope around a series of database operations."""
    with writing_session_lock if write else nullcontext():
        session = Session(expire_on_commit=False)
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()


def init_db(app_data_dir: str, db_name: str, debug=False):
    """Set up database connection and run migrations if needed."""
    url = f"sqlite:///{app_data_dir}/{db_name}"

    logger.info("Configuring database...")
    engine = create_engine(url, echo=debug, connect_args={"check_same_thread": False})
    Session.configure(bind=engine)
    Base.metadata.bind = engine

    cfg = Config()
    cfg.set_main_option("script_location", resource_path("alembic"))
    cfg.set_main_option("sqlalchemy.url", url)
    command.upgrade(cfg, "head")
    logger.info("Database configured and up to date")


def init_app_data_dir(app_name: str, temporary=False):
    """Create application data directory if it doesn't exist, and return its
    path.
    """
    if temporary:
        app_data_dir = tempfile.mkdtemp(suffix=app_name)
    else:
        app_data_dir = user_data_dir(appname=app_name)
    if not os.path.exists(app_data_dir):
        os.makedirs(app_data_dir)
    logger.info(f"Application data directory that will be used: {app_data_dir}")
    return app_data_dir


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


def get_or_create(session: Session_, model, **kwargs):
    """Fetch first row matching given criteria or create it if none was found."""
    instance = session.query(model).filter_by(**kwargs).first()
    if not instance:
        instance = model(**kwargs)
        session.add(instance)
    return instance
