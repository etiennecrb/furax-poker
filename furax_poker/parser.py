import datetime
import logging
import re
from typing import cast, Iterable, List, Optional, TypedDict, Union


logger = logging.getLogger(__name__)


class RawPlayer(TypedDict):
    name: str
    seat: int
    stack: float


class RawAction(TypedDict):
    type: str
    value: Optional[Union[float, List[str]]]
    player_name: str
    all_in: bool


class RawRound(TypedDict):
    actions: List[RawAction]


class RawHand(TypedDict):
    hand_id: str
    start_datetime: datetime.datetime
    game_type: str
    variant: str
    tournament_name: Optional[str]
    tournament_id: Optional[str]
    table_name: str
    buy_in: Optional[float]
    level: Optional[int]
    ante: float
    small_blind: float
    big_blind: float
    max_seats: int
    button_seat: int
    players: List[RawPlayer]
    rounds: List[RawRound]
    board: List[str]
    hero_name: Optional[str]
    hero_cards: Optional[List[str]]


def parse(txt: Iterable[str]):
    """Parse hands from a hand history file."""
    # A hand in history file is formatted like this:
    #   1. hand header
    #   2. table info
    #   3. players and seats
    #   4. ante & blinds
    #   5. cards dealt to hero (if any)
    #   6. rounds
    #   7. hand summary
    # Each round is composed by a header line like *** FLOP *** followed by
    # one line per player action. Here parsing is handled like a state machine
    # so the proper function is called to parse each line, the result is
    # "reduced" in a dict.
    hand = None
    lines = []  # Accumulated lines for current parsed hand (debug)

    new_hand_regex = r"^Winamax Poker .+ HandId: #.+"
    dealt_cards_regex = r"^Dealt to .+ \[.+\]"
    first_round_regex = r"^\*\*\* ANTE/BLINDS \*\*\*"
    new_round_regex = r"^\*\*\* (PRE\-FLOP|FLOP|TURN|RIVER|SHOW DOWN) \*\*\*"
    summary_regex = r"^\*\*\* SUMMARY \*\*\*"
    board_regex = r"^Board: \[.*\]"
    end_regex = r"^\s*$"

    state = "hand start"
    for line in txt:
        lines.append(line)
        try:
            if state == "hand start":
                # Wait for first line of hand
                if re.match(new_hand_regex, line):
                    # Nothing to do yet, just go to next state
                    state = "table"
                    lines = [line]
            elif state == "table":
                # Parse generic hand data and go to next state
                hand = parse_hand_header(lines)
                state = "player"
            elif state == "player":
                if re.match(first_round_regex, line):
                    # This indicates the start of first round
                    cast(RawHand, hand)["rounds"].append(parse_round_line(line))
                    state = "round"
                else:
                    # Parse players as long as there are some
                    cast(RawHand, hand)["players"].append(parse_player_line(line))
            elif state == "round":
                if re.match(summary_regex, line):
                    # This indicates the end of hand and start of summary
                    state = "summary"
                elif re.match(dealt_cards_regex, line):
                    # This line contains cards dealt to hero
                    cast(RawHand, hand).update(parse_hero_line(line))
                elif re.match(new_round_regex, line):
                    # This indicates the start of another round
                    cast(RawHand, hand)["rounds"].append(parse_round_line(line))
                else:
                    # In any other case, it's a round action
                    current_round = cast(RawHand, hand)["rounds"][-1]
                    current_round["actions"].append(parse_action_line(line))
            elif state == "summary":
                # Just parse the board from summary and wait for next hand
                # Board line doesn't exist when players didn't see the flop
                if re.match(board_regex, line):
                    cast(RawHand, hand)["board"] = parse_board_line(line)
                if re.match(end_regex, line):
                    yield hand
                    state = "hand start"
        except Exception:
            logger.exception(
                "Exception occured while parsing file", extra={"lines": lines}
            )
            state = "hand start"


def parse_hand_header(lines: List[str]) -> RawHand:
    """Extract data from first lines of hand in history file."""
    hand_regex = (
        r"Winamax Poker - (?P<game>.+)"
        r" - HandId: #(?P<hand_id>.+)"
        r" - (?P<variant>.+)"
        r" \((?P<blinds>.+)\)"
        r" - (?P<date>.+)"
    )
    table_regex = (
        r"Table: \'(?P<name>.+)\'"
        r" (?P<max_seats>\d+)-max"
        r" \((?P<money>.+)\)"
        r" Seat #(?P<button>\d+) is the button"
    )
    if len(lines) != 2:
        raise Exception("bad header length")
    hand_match = re.match(hand_regex, lines[0])
    table_match = re.match(table_regex, lines[1])
    if hand_match is None:
        raise Exception("bad hand line")
    if table_match is None:
        raise Exception("bad table line")

    hand_id = hand_match.group("hand_id")
    start_datetime = None
    game_type = None
    variant = None
    tournament_name = None
    tournament_id = None
    table_name = None
    buy_in = None
    level = None
    ante = None
    small_blind = None
    big_blind = None
    max_seats = None
    button_seat = None

    # Parse hand start datetime
    try:
        raw_dt = hand_match.group("date")
        dt_format = "%Y/%m/%d %H:%M:%S %Z"
        start_datetime = datetime.datetime.strptime(raw_dt, dt_format)
    except ValueError:
        raise Exception("bad start datetime")

    if hand_match.group("variant") != "Holdem no limit":
        raise Exception("Cannot parse a variant other than NO LIMIT HOLDEM")
    variant = "NO_LIMIT_HOLDEM"

    # Parse blinds
    raw_blinds = hand_match.group("blinds")
    blinds = [float(clean_price(x)) for x in raw_blinds.split("/")]
    if len(blinds) == 2:
        # If there are only two blinds, it means there's no ante
        blinds.insert(0, 0)
    ante, small_blind, big_blind = blinds

    game = hand_match.group("game")
    if game == "CashGame":
        game_type = "CASH_GAME"
    else:
        tournament_regex = (
            r'Tournament "(?P<name>.+)"'
            r" buyIn: (?P<buy_in>\S+)( \+ (?P<rake>.+))?"
            r" level: (?P<level>.+)?"
        )
        match = re.match(tournament_regex, game)
        if match:
            game_type = "TOURNAMENT"
            tournament_name = match.group("name")

            level_str = match.group("level")
            if level_str is not None:
                level = int(level_str)

            # Parse buy-in, rake included if any and round result
            buy_in = float(clean_price(match.group("buy_in")))
            rake = match.group("rake")
            if rake:
                buy_in += float(clean_price(rake))
            buy_in = round(buy_in, 2)
        else:
            match = re.match(r'Go Fast "(.+)"', game)
            if match:
                game_type = "CASH_GAME"
            else:
                raise Exception("no game type found")

    table_name = table_match.group("name")
    max_seats = int(table_match.group("max_seats"))
    button_seat = int(table_match.group("button"))

    if game_type == "TOURNAMENT":
        match = re.match(r"(.+)\((?P<id>.+)\)#(.+)", table_name)
        if match is None:
            raise Exception("bad table line for tournament")
        tournament_id = match.group("id")

    return {
        "hand_id": hand_id,
        "start_datetime": start_datetime,
        "game_type": game_type,
        "variant": variant,
        "tournament_name": tournament_name,
        "tournament_id": tournament_id,
        "table_name": table_name,
        "buy_in": buy_in,
        "level": level,
        "ante": ante,
        "small_blind": small_blind,
        "big_blind": big_blind,
        "max_seats": max_seats,
        "button_seat": button_seat,
        "players": [],
        "rounds": [],
        "board": [],
        "hero_name": None,
        "hero_cards": None,
    }


def parse_player_line(line: str) -> RawPlayer:
    line_regex = r"Seat (?P<seat>\d+)\: (?P<name>.+) \((?P<stack>.+)\)"
    match = re.match(line_regex, line)
    if match is None:
        raise Exception("bad player line")

    return {
        "name": match.group("name"),
        "seat": int(match.group("seat")),
        "stack": float(clean_price(match.group("stack"))),
    }


def parse_round_line(line: str) -> RawRound:
    return {"actions": []}


def parse_board_line(line: str) -> List[str]:
    line_regex = r"Board: \[(?P<cards>.*)\]"
    match = re.match(line_regex, line)
    if match is None:
        raise Exception("bad board line")
    return parse_cards(match.group("cards"))


def parse_hero_line(line: str):
    line_regex = r"Dealt to (?P<name>.+) \[(?P<cards>.+)\]"
    match = re.match(line_regex, line)
    if match is None:
        raise Exception("bad hero line")
    return {
        "hero_name": match.group("name"),
        "hero_cards": parse_cards(match.group("cards")),
    }


def parse_action_line(line: str) -> RawAction:
    line_regex = (
        r"(?P<player_name>.+) "
        r"(?P<type>posts ante|posts small blind|posts big blind|"
        r"checks|bets|calls|raises|folds|collected|shows)"
        r"(?P<props>.*)"
    )
    match = re.match(line_regex, line)
    if match is None:
        raise Exception("bad action line")

    player_name = match.group("player_name")
    props = match.group("props")
    all_in = "and is all-in" in props
    action = match.group("type")
    value: Optional[Union[float, List[str]]]
    if action == "posts ante":
        type = "ANTE"
        value = float(clean_price(props))
    elif action == "posts small blind":
        type = "SMALL_BLIND"
        value = float(clean_price(props))
    elif action == "posts big blind":
        type = "BIG_BLIND"
        value = float(clean_price(props))
    elif action == "checks":
        type = "CHECK"
        value = None
    elif action == "calls":
        type = "CALL"
        value = float(clean_price(props))
    elif action == "bets":
        type = "BET"
        value = float(clean_price(props))
    elif action == "raises":
        type = "RAISE"
        value = float(clean_price(props.split(" to ")[0]))
    elif action == "folds":
        type = "FOLD"
        value = None
    elif action == "collected":
        type = "COLLECT"
        value = float(clean_price(props))
    elif action == "shows":
        type = "SHOW"
        match = re.match(r" \[(?P<cards>.+)\]", props)
        if match is None:
            raise Exception("bad show action line")
        value = parse_cards(match.group("cards"))

    return {"type": type, "value": value, "player_name": player_name, "all_in": all_in}


def parse_cards(s: str) -> List[str]:
    return [c for c in s.split(" ") if len(c)]


def clean_price(s: str) -> str:
    """Remove currency from string."""
    match = re.match(r"\D*([\d\.]+)", s)
    if match is None:
        raise Exception("bad price")
    return match.group(1)
