from dataclasses import dataclass
import enum
from typing import Any, Optional, TypeVar, Type, TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, func, Integer, UniqueConstraint

# https://github.com/dropbox/sqlalchemy-stubs/issues/114
if TYPE_CHECKING:
    from sqlalchemy.sql.type_api import TypeEngine

    T = TypeVar("T")

    class Enum(TypeEngine[T]):
        def __init__(self, enum: Type[T], **kwargs: Any) -> None:
            ...


else:
    from sqlalchemy import Enum
from sqlalchemy.orm import relationship, Session

from furax_poker.db import Base
from furax_poker.hand import Action, Hand, Seat, Table
from furax_poker.settings import TimeScope


@enum.unique
class FactType(enum.Enum):
    # Player took part in a hand
    HAND_PLAYED = enum.auto()

    # Preflop action type
    PREFLOP_CHECK = enum.auto()
    PREFLOP_CALL = enum.auto()
    PREFLOP_BET = enum.auto()
    PREFLOP_RAISE = enum.auto()
    PREFLOP_FOLD = enum.auto()

    # Flop action type
    FLOP_CHECK = enum.auto()
    FLOP_CALL = enum.auto()
    FLOP_BET = enum.auto()
    FLOP_RAISE = enum.auto()
    FLOP_FOLD = enum.auto()

    # Turn action type
    TURN_CHECK = enum.auto()
    TURN_CALL = enum.auto()
    TURN_BET = enum.auto()
    TURN_RAISE = enum.auto()
    TURN_FOLD = enum.auto()

    # River action type
    RIVER_CHECK = enum.auto()
    RIVER_CALL = enum.auto()
    RIVER_BET = enum.auto()
    RIVER_RAISE = enum.auto()
    RIVER_FOLD = enum.auto()

    # Player didn't put money in pot preflop
    VPIP_CHANCE = enum.auto()

    # Player voluntary put money in pot preflop
    VPIP = enum.auto()

    # Player raised while putting money in pot preflop
    PFR = enum.auto()

    # Player missed a cbet
    CBET_CHANCE = enum.auto()

    # Player cbet
    CBET = enum.auto()

    # Player didn't fold to another player's cbet
    FOLD_TO_CBET_CHANCE = enum.auto()

    # Player fold to another player's cbet
    FOLD_TO_CBET = enum.auto()

    def __str__(self):
        return self.name


class Fact(Base):
    """Represents a fact about a player action."""

    __table_args__ = (UniqueConstraint("action_id", "type"),)

    type = Column(Enum(FactType), nullable=False, index=True)
    action_id = Column(
        Integer, ForeignKey("action.id", ondelete="CASCADE"), nullable=False, index=True
    )
    action = relationship(Action, back_populates="facts")

    def __repr__(self):
        return (
            f"{self.__class__.__name__}(id={self.id}, type={self.type}) "
            f"linked to {self.action!r}"
        )


@dataclass(frozen=True)
class FactMetrics(object):
    """Represents metrics computed from a set of facts.

    This object holds occurences of fact by type to ease access to derived metrics such
    as rates or specific poker statistics.
    """

    hand_played: int = 0
    preflop_check: int = 0
    preflop_call: int = 0
    preflop_bet: int = 0
    preflop_raise: int = 0
    preflop_fold: int = 0
    flop_check: int = 0
    flop_call: int = 0
    flop_bet: int = 0
    flop_raise: int = 0
    flop_fold: int = 0
    turn_check: int = 0
    turn_call: int = 0
    turn_bet: int = 0
    turn_raise: int = 0
    turn_fold: int = 0
    river_check: int = 0
    river_call: int = 0
    river_bet: int = 0
    river_raise: int = 0
    river_fold: int = 0
    vpip_chance: int = 0
    vpip: int = 0
    pfr: int = 0
    cbet_chance: int = 0
    cbet: int = 0
    fold_to_cbet_chance: int = 0
    fold_to_cbet: int = 0

    @property
    def vpip_obs(self) -> int:
        return self.vpip + self.vpip_chance

    @property
    def vpip_rate(self) -> Optional[float]:
        if self.vpip_obs > 0:
            return self.vpip / self.vpip_obs
        return None

    @property
    def pfr_rate(self) -> Optional[float]:
        if self.vpip_obs > 0:
            return self.pfr / self.vpip_obs
        return None

    @property
    def af(self) -> Optional[float]:
        calls = self.preflop_call + self.flop_call + self.turn_call + self.river_call
        if calls > 0:
            bets = (
                self.preflop_bet
                + self.preflop_raise
                + self.flop_bet
                + self.flop_raise
                + self.turn_bet
                + self.turn_raise
                + self.river_bet
                + self.river_raise
            )
            return bets / calls
        return None

    @property
    def cbet_obs(self) -> int:
        return self.cbet + self.cbet_chance

    @property
    def cbet_rate(self) -> Optional[float]:
        if self.cbet_obs > 0:
            return self.cbet / self.cbet_obs
        return None

    @property
    def fold_to_cbet_obs(self) -> int:
        return self.fold_to_cbet + self.fold_to_cbet_chance

    @property
    def fold_to_cbet_rate(self) -> Optional[float]:
        if self.fold_to_cbet_obs > 0:
            return self.fold_to_cbet / self.fold_to_cbet_obs
        return None

    @classmethod
    def from_seat(
        cls, session: Session, seat: Seat, time_scope: Optional[TimeScope] = None
    ) -> "FactMetrics":
        """Create a fact metrics object from all actions of player at given seat."""
        query = (
            session.query(Fact.type, func.count("*"))
            .join(Action)
            .join(Seat)
            .join(Hand)
            .join(Table)
            .filter(Seat.player == seat.player)
            .group_by(Fact.type)
        )

        if time_scope is not None:
            if time_scope == TimeScope.CURRENT_SESSION:
                if seat.hand.table.tournament is not None:
                    filter_clause = Table.tournament == seat.hand.table.tournament
                else:
                    filter_clause = Hand.table == seat.hand.table
                query = query.filter(filter_clause)

        return cls(**{fact_type.name.lower(): count for fact_type, count in query})
