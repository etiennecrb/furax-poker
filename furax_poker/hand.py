import enum
from itertools import zip_longest
from typing import Any, List, Optional, TypeVar, Type, TYPE_CHECKING

from sqlalchemy import (
    Boolean,
    CheckConstraint,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    UniqueConstraint,
)

# https://github.com/dropbox/sqlalchemy-stubs/issues/114
if TYPE_CHECKING:
    from sqlalchemy.sql.type_api import TypeEngine

    T = TypeVar("T")

    class Enum(TypeEngine[T]):
        def __init__(self, enum: Type[T], **kwargs: Any) -> None:
            ...


else:
    from sqlalchemy import Enum
from sqlalchemy.orm import relationship

from furax_poker.db import Base

if TYPE_CHECKING:
    from furax_poker.fact import Fact  # noqa:F401


class Room(enum.Enum):
    """Represents a poker room (Winamax, PokerStart...)."""

    WINAMAX = 0

    def __str__(self):
        return self.name


class Variant(enum.Enum):
    """Represents a poker variant (No Limit Hold'em, Stud...)."""

    NO_LIMIT_HOLDEM = 0

    def __str__(self):
        return self.name


class GameType(enum.Enum):
    """Represents a poker game type (Cash game, tournament...)."""

    CASH_GAME = 0
    TOURNAMENT = 1

    def __str__(self):
        return self.name


class Hand(Base):
    """Represents a hand of poker."""

    __table_args__ = (UniqueConstraint("table_id", "key"),)

    key = Column(String)
    start_datetime = Column(DateTime(), nullable=False, index=True)
    variant = Column(Enum(Variant), nullable=False, index=True)
    game_type = Column(Enum(GameType), nullable=False, index=True)
    buy_in = Column(Float, nullable=True, index=True)
    level = Column(Integer, nullable=True)
    ante = Column(Float, nullable=True)
    small_blind = Column(Float, nullable=False)
    big_blind = Column(Float, nullable=False)
    max_seats = Column(Integer, nullable=False)
    analyzed = Column(Boolean, nullable=False, default=False, server_default="FALSE")
    table_id = Column(Integer, ForeignKey("table.id"), nullable=False, index=True)
    table = relationship("Table", back_populates="hands", lazy="joined")
    board = relationship("Board", back_populates="hand", uselist=False, lazy="joined")
    seats: List["Seat"] = relationship(
        "Seat", back_populates="hand", lazy="joined", order_by="Seat.order"
    )  # type: ignore
    rounds: List["Round"] = relationship(
        "Round", back_populates="hand", lazy="joined", order_by="Round.order"
    )  # type: ignore

    def __repr__(self):
        return (
            f"{self.__class__.__name__}(id={self.id}, key={self.key}), {self.table!r}"
        )


class Tournament(Base):
    """Represents a poker tournament."""

    __table_args__ = (UniqueConstraint("room", "key"),)

    key = Column(String)
    room = Column(Enum(Room), nullable=False, index=True)
    name = Column(String, nullable=False, index=True)
    tables: List["Table"] = relationship(
        "Table", back_populates="tournament"
    )  # type: ignore

    def __repr__(self):
        return f"{self.__class__.__name__}(id={self.id}, key={self.key})"


class Table(Base):
    """Represents a poker table."""

    __table_args__ = (UniqueConstraint("room", "name"),)

    room = Column(Enum(Room), nullable=False, index=True)
    name = Column(String, nullable=False, index=True)
    tournament_id = Column(
        Integer, ForeignKey("tournament.id"), nullable=True, index=True
    )
    tournament: Optional[Tournament] = relationship(
        "Tournament", back_populates="tables", lazy="joined"
    )  # type: ignore
    hands: List["Hand"] = relationship("Hand", back_populates="table")  # type: ignore

    def __repr__(self):
        tournament = f", {self.tournament!r}" if self.tournament else ""
        return f"{self.__class__.__name__}(id={self.id}, name={self.name}){tournament}"


class Board(Base):
    """Represents community cards of a hand."""

    flop_1 = Column(String, nullable=True)
    flop_2 = Column(String, nullable=True)
    flop_3 = Column(String, nullable=True)
    turn = Column(String, nullable=True)
    river = Column(String, nullable=True)
    hand_id = Column(
        Integer,
        ForeignKey("hand.id", ondelete="CASCADE"),
        nullable=False,
        unique=True,
        index=True,
    )
    hand = relationship("Hand", back_populates="board")

    @classmethod
    def from_list(cls, cards):
        streets = ["flop_1", "flop_2", "flop_3", "turn", "river"]
        return cls(**dict(zip_longest(streets, cards)))

    def __repr__(self):
        details = [
            card
            for card in [self.flop_1, self.flop_2, self.flop_3, self.turn, self.river]
            if card is not None
        ]
        return f"{self.__class__.__name__}(id={self.id}, cards=[{', '.join(details)}])"


class SeatPosition(enum.Enum):
    """Represents seat order of play during a hand.

    Note that all positions don't always exist, it depends on the number of
    players seated at table. Each position is determined by its order and the
    player count required for it to be defined. The first positions to exist
    are the blinds, then button and cutoff, then "under the gun" positions and
    at last middle positions.
    """

    SMALL_BLIND = (0, 1)
    BIG_BLIND = (1, 2)
    BUTTON = (9, 3)
    CUTOFF = (8, 4)
    UTG_1 = (2, 5)
    UTG_2 = (3, 6)
    UTG_3 = (4, 7)
    MP_1 = (5, 8)
    MP_2 = (6, 9)
    MP_3 = (7, 10)

    def __init__(self, order: int, required_player_count: int):
        self.order = order
        self.required_player_count = required_player_count

    @classmethod
    def list_available(
        cls, player_count=10, dead_small_blind=False
    ) -> List["SeatPosition"]:
        """Return the list of available positions sorted by order of play.

        Available positions are determined by number of player and if small
        blind exist (in some particular cases, the small blind is skipped to
        ensure no players skip the big blind).
        """
        start = 1 if not dead_small_blind else 2
        available_positions = []

        # Use required player count to keep only available positions
        for i in range(start, start + player_count):
            try:
                pos = next(s for s in SeatPosition if s.required_player_count == i)
            except StopIteration:
                msg = f"Impossible seats configuration: {player_count} players"
                if dead_small_blind:
                    msg += " with dead small blind"
                raise ValueError(msg)
            available_positions.append(pos)
        return sorted(available_positions, key=lambda p: p.order)

    def __str__(self):
        return self.name


class Seat(Base):
    """Represents a player seated at a table for a particular hand."""

    __tableargs__ = (
        UniqueConstraint("hand_id", "number"),
        # Hero cards must be defined
        CheckConstraint("NOT is_hero OR (card_1 IS NOT NULL AND card_2 IS NOT NULL)"),
    )

    number = Column(Integer, nullable=False, index=True)
    stack = Column(Float, nullable=False)
    position = Column(Enum(SeatPosition), nullable=False, index=True)
    order = Column(Integer, nullable=False, index=True)
    is_hero = Column(Boolean, nullable=False)
    card_1 = Column(String, nullable=True, index=True)
    card_2 = Column(String, nullable=True, index=True)
    hand_id = Column(
        Integer, ForeignKey("hand.id", ondelete="CASCADE"), nullable=False, index=True
    )
    hand = relationship("Hand", back_populates="seats")
    player_id = Column(Integer, ForeignKey("player.id"), nullable=False, index=True)
    player = relationship("Player", back_populates="seats", lazy="joined")
    actions: List["Action"] = relationship(
        "Action", back_populates="seat"
    )  # type: ignore

    def __repr__(self):
        return (
            f"{self.__class__.__name__}(id={self.id}, position={self.position}) "
            f"with {self.player!r}"
        )


class Player(Base):
    """Represents a player in a room."""

    __table_args = (UniqueConstraint("room", "name"),)

    room = Column(Enum(Room), nullable=False, index=True)
    name = Column(String, nullable=False, index=True)
    seats: List["Seat"] = relationship("Seat", back_populates="player")  # type: ignore

    def __repr__(self):
        return f"{self.__class__.__name__}(id={self.id}, name={self.name})"


class RoundType(enum.Enum):
    """Represents round type (blinds, preflop...).

    Enum values are ordered as round type are in a hand.
    """

    BLINDS = 0
    PREFLOP = 1
    FLOP = 2
    TURN = 3
    RIVER = 4
    SHOWDOWN = 5

    @property
    def order(self) -> int:
        return self.value

    def __str__(self):
        return self.name


class Round(Base):
    """Represents a round of poker hand."""

    type = Column(Enum(RoundType), nullable=False, index=True)
    order = Column(Integer, nullable=False, index=True)
    actions: List["Action"] = relationship(
        "Action", back_populates="round", lazy="joined"
    )  # type: ignore
    hand_id = Column(
        Integer, ForeignKey("hand.id", ondelete="CASCADE"), nullable=False, index=True
    )
    hand = relationship("Hand", back_populates="rounds")

    def __repr__(self):
        return (
            f"{self.__class__.__name__}(id={self.id}, type={self.type}) "
            f"of {self.hand!r}"
        )


class ActionType(enum.Enum):
    """Represents the type of a player action (call, fold...)."""

    ANTE = 0
    SMALL_BLIND = 1
    BIG_BLIND = 2
    CHECK = 3
    CALL = 4
    BET = 5
    RAISE = 6
    FOLD = 7
    SHOW = 8
    COLLECT = 9

    def __str__(self):
        return self.name


class Action(Base):
    """Represents an action of a player during a round."""

    __table_args__ = (UniqueConstraint("round_id", "order"),)

    order = Column(Integer, nullable=False)
    type = Column(Enum(ActionType), nullable=False, index=True)
    value = Column(Float, nullable=True)
    all_in = Column(Boolean, nullable=False)
    round_id = Column(
        Integer, ForeignKey("round.id", ondelete="CASCADE"), nullable=False, index=True
    )
    round = relationship("Round", back_populates="actions")
    seat_id = Column(
        Integer, ForeignKey("seat.id", ondelete="CASCADE"), nullable=False, index=True
    )
    seat = relationship("Seat", back_populates="actions", lazy="joined")
    facts: List["Fact"] = relationship("Fact", back_populates="action")  # type: ignore

    def __repr__(self):
        details = [f"type={self.type}"]
        if self.value is not None:
            details.append(f"value={round(self.value, 2)}")
        if self.all_in:
            details.append("all_in=True")
        return (
            f"{self.__class__.__name__}(id={self.id}, {', '.join(details)}) "
            f"by {self.seat!r} during {self.round!r}"
        )
