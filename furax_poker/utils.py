import os
import sys


def resource_path(path: str) -> str:
    """Returns path for a resource whether the app is bundled or not.

    See https://pyinstaller.readthedocs.io/en/stable/runtime-information.html
    for more information.
    """
    return os.path.join(getattr(sys, "_MEIPASS", ""), path)
