from typing import TYPE_CHECKING

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QCheckBox, QLabel, QWidget

from furax_poker.db import session_scope
from furax_poker.gui.utils import UiAttrs
from furax_poker.hand import Table
from furax_poker.settings import ApplicationSettings
from furax_poker.utils import resource_path

if TYPE_CHECKING:
    from furax_poker.gui.application import Application


class TableListItem(QWidget, UiAttrs):
    """Display table information and allow user to start and stop HUD for this table."""

    _label: QLabel
    _checkBox: QCheckBox

    def __init__(self, app: "Application", table: Table):
        super().__init__()
        self.app = app
        self.table = table
        self.loadUi(resource_path("data/ui/table_list_item.ui"))
        self._label.setText(table.name)
        self._checkBox.stateChanged.connect(self._onCheckBoxStateChanged)

        # Check settings and automatically start HUD if this setting is on
        with session_scope() as session:
            settings = session.query(ApplicationSettings).first()
            if settings and settings.autostart_hud:
                self._checkBox.setCheckState(Qt.Checked)

    def _onCheckBoxStateChanged(self, state: Qt.CheckState):
        if state == Qt.Checked:
            self.app.startHud(self.table)
        else:
            self.app.stopHud(self.table)
