from typing import TYPE_CHECKING

from PyQt5.QtWidgets import QLabel, QWidget

from furax_poker.gui.utils import UiAttrs
from furax_poker.utils import resource_path

if TYPE_CHECKING:
    from furax_poker.gui.application import Application


class EmptyView(QWidget, UiAttrs):
    _label: QLabel

    def __init__(self, app: "Application"):
        super().__init__()
        self.app = app
        self.loadUi(resource_path("data/ui/empty_view.ui"))
        self._label.linkActivated.connect(self.app.openSettingsDialog)
