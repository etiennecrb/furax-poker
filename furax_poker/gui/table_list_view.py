from typing import TYPE_CHECKING

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QVBoxLayout, QWidget

from furax_poker.gui.table_list_item import TableListItem
from furax_poker.gui.utils import UiAttrs
from furax_poker.hand import Table
from furax_poker.utils import resource_path

if TYPE_CHECKING:
    from furax_poker.gui.application import Application


class TableListView(QWidget, UiAttrs):
    _layout: QVBoxLayout

    def __init__(self, app: "Application"):
        super().__init__()
        self.app = app
        self.loadUi(resource_path("data/ui/table_list_view.ui"))
        self.app.tracker.table_joined.connect(self._on_refresh)
        self._layout.setAlignment(Qt.AlignTop)

    def _on_refresh(self, table: Table):
        self._layout.addWidget(TableListItem(self.app, table))
