import logging
from typing import cast, List, Optional

from sqlalchemy.orm import lazyload, Session

from furax_poker.hand import (
    Action,
    ActionType,
    Board,
    GameType,
    Hand,
    Player,
    Room,
    Round,
    RoundType,
    Seat,
    SeatPosition,
    Table,
    Tournament,
    Variant,
)
from furax_poker.parser import RawHand

logger = logging.getLogger(__name__)


def load_hand(session: Session, room: Room, raw_hand: RawHand):
    """Load a raw hand and its associated objects into given session.

    Returns the hand that was added to session.
    """
    game_type = GameType[raw_hand["game_type"]]

    # Get or create tournament if needed
    tournament: Optional[Tournament] = None
    if game_type is GameType.TOURNAMENT:
        tournament = (
            session.query(Tournament)
            .filter_by(room=room, key=raw_hand["tournament_id"])
            .first()
        )
        if not tournament:
            tournament = Tournament(
                room=room,
                key=raw_hand["tournament_id"],
                name=raw_hand["tournament_name"],
            )  # type: ignore
            session.add(tournament)

    # Get or create table
    table = (
        session.query(Table).filter_by(room=room, name=raw_hand["table_name"]).first()
    )
    if not table:
        table = Table(
            room=room, name=raw_hand["table_name"], tournament=tournament
        )  # type: ignore
        session.add(table)

    # Do not try to reload an existing hand
    existing_hand = (
        session.query(Hand)
        .options(lazyload("*"))
        .filter_by(table=table, key=raw_hand["hand_id"])
        .first()
    )
    if existing_hand:
        return

    board = Board.from_list(raw_hand["board"])
    session.add(board)

    hand = Hand(
        key=raw_hand["hand_id"],
        start_datetime=raw_hand["start_datetime"],
        variant=Variant[raw_hand["variant"]],
        game_type=game_type,
        buy_in=raw_hand["buy_in"],
        level=raw_hand["level"],
        ante=raw_hand["ante"],
        small_blind=raw_hand["small_blind"],
        big_blind=raw_hand["big_blind"],
        max_seats=raw_hand["max_seats"],
        table=table,
        board=board,
    )
    session.add(hand)

    # Prepare data to determine seat positions
    player_count = len(raw_hand["players"])
    button_seat = raw_hand["button_seat"]
    dead_small_blind = True
    for raw_action in raw_hand["rounds"][0]["actions"]:
        if ActionType[raw_action["type"]] is ActionType.SMALL_BLIND:
            dead_small_blind = False
    seat_positions = SeatPosition.list_available(player_count, dead_small_blind)

    # Order players from small blind to button
    ordered_players = sorted(
        raw_hand["players"],
        key=lambda p: (p["seat"] - button_seat - 1) % hand.max_seats,
    )

    existing_players = session.query(Player).filter(
        Player.room == room,
        Player.name.in_([raw_player["name"] for raw_player in ordered_players]),
    )
    existing_player_by_name = {p.name: p for p in existing_players}

    # Map of seat by player name, used when creating actions
    seat_by_player_name = {}
    for i, raw_player in enumerate(ordered_players):
        # Get or create player
        player = existing_player_by_name.get(raw_player["name"])
        if not player:
            player = Player(room=room, name=raw_player["name"])
            session.add(player)

        seat = Seat(
            number=raw_player["seat"],
            stack=raw_player["stack"],
            is_hero=raw_hand.get("hero_name", None) == player.name,
            position=seat_positions[i],
            order=seat_positions[i].order,
            card_1=None,
            card_2=None,
            player=player,
            hand=hand,
        )

        # Assign hero cards
        if seat.is_hero and raw_hand["hero_cards"]:
            hero_cards = cast(List[str], raw_hand["hero_cards"])
            seat.card_1 = hero_cards[0]
            seat.card_2 = hero_cards[1]

        session.add(seat)
        seat_by_player_name[player.name] = seat

    # Create rounds and actions
    for i, raw_round in enumerate(raw_hand["rounds"]):
        round_type = RoundType(i)
        round = Round(type=round_type, order=round_type.order, hand=hand)
        session.add(round)
        for order, raw_action in enumerate(raw_round["actions"]):
            seat = seat_by_player_name[raw_action["player_name"]]
            type = ActionType[raw_action["type"]]
            value = raw_action["value"]

            # If showdown, assign cards to corresponding seat
            if type is ActionType.SHOW:
                cards = cast(List[str], value)
                seat.card_1 = cards[0]
                seat.card_2 = cards[1]
                value = None

            action = Action(
                type=type,
                order=order,
                value=cast(Optional[float], value),
                all_in=raw_action["all_in"],
                round=round,
                seat=seat,
            )
            session.add(action)

    return hand
