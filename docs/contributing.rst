.. _contributing:

============
Contributing
============

Furax Poker is a community-maintained project and we happily accept
contributions in the form of bug reports, feature requests, and pull requests.
Furax Poker is open-source under the GPL_ licence version 3 and is hosted on
GitLab_.

.. _GPL: https://gitlab.com/etiennecrb/furax-poker/-/raw/master/LICENSE.txt

Questions and feature requests
------------------------------

Feel free to open an issue on `GitLab issue tracker`_ with the appropriate
label.

Reporting issues
----------------

File a bug on `GitLab issue tracker`_. To help us figure out what's going on,
please include a much information as possible, such as:

- Furax Poker version,
- operating system,
- a minimal case that can reproduce the issue.

Merge requests
--------------

If you wish to add a new feature or fix a bug:

- Set up your :ref:`development environment <dev-guide-getting-started>`.
- `Check for open issues <GitLab issue tracker_>`_ or
  open a fresh issue to start a discussion around a feature idea or a bug. It'll
  allow chance to talk it over with the owners and validate your approach.
  Discussing the design up front helps to ensure that we're ready to accept your
  work.
- Clone the `Furax Poker repository <GitLab_>`_, create a new branch to start
  making your changes:

.. code-block:: text

   git checkout -b my-feature master

- Write a test which shows that the bug was fixed or that the feature works as
  expected.
- Follow our :ref:`contributing-coding-guidelines`.
- As described in the :ref:`dev-guide`, run the full test suite to ensure that
  all tests pass and format your code with black.
- Commit your changes using a descriptive commit message that follows our
  :ref:`contributing-commit-message-conventions`.
- Push your branch to GitLab and open a merge request.

.. code-block:: text

   git push -u origin my-feature

.. _contributing-coding-guidelines:

Coding guidelines
-----------------

To ensure consistency throughout the source code, keep these guidelines in mind
as you are working:

- Follow `PEP8 <https://www.python.org/dev/peps/pep-0008/>`_.
- All features or bug fixes should be tested by unit-tests.
- Add `type hints <https://docs.python.org/3.8/library/typing.html>`_.
- All public API methods should be documented.
- Make sure each commit reflects a single purpose: the fixing of a specific bug,
  the addition of a new feature, or some particular task.

Style guide is enforced using `Flake8 <https://flake8.pycqa.org/en/latest/>`_.
Types are checked using `mypy <http://mypy-lang.org/>`_. Code is formatted with
`black <https://black.readthedocs.io/en/stable/>`_.

.. _contributing-commit-message-conventions:

Commit message conventions
--------------------------

We try to follow  the `Conventional Commits
<https://www.conventionalcommits.org>`_ specification to ensure consistency and
readability of the commit history.

Each commit message consists of a header and an optional body. The header has a
special format that includes a type, an optional scope and a subject:

.. code-block:: text

    <type>(<scope>): <subject>
    <BLANK LINE>
    <body>

Any line of the commit message cannot be longer than 72 characters. This allows
the message to be easier to read on GitLab as well as in various git tools.

Samples:

.. code-block:: text

    fix(tracker): use utf-8, catch file reading errors

.. code-block:: text

    feat(hud): make HUD windows follow active window

.. code-block:: text

    chore: bump version 1.0.0

Type should be one of the following:

- chore: changes that affect the build system, external dependencies, CI,
- feat: a new feature
- fix: a bug fix
- refactor: a code change that neither fixes a bug nor adds a feature
- test: adding missing tests or correcting existing tests

Scope should be the name of the package or feature impacted by commits changes,
this is a bit subjective and the following list is just an indication of some
accepted scopes:

- db
- hud
- tracker
- ci
- docs
- ...

The subject contains a succinct description of the change. Don't capitalize the
first letter and don't put a dot (.) at the end. For subject and body, use the
imperative, present tense: "change" not "changed" nor "changes". If your commit
is linked to an issue that contains meaningful information, refer the issue id
in the body message.

.. _GitLab issue tracker: https://gitlab.com/etiennecrb/furax-poker/issues
.. _GitLab: https://gitlab.com/etiennecrb/furax-poker
