.. _index:

===========
Furax Poker
===========

Furax Poker is an open source poker HUD (head-up display) application. It
monitors your hand history folder in real time to display statistics about your
opponents. Furax Poker works on Linux and Windows, the only supported poker
room is Winamax but others could easily be added.

.. image:: screenshot.png

.. toctree::
   :maxdepth: 3
   :hidden:

   user-guide/index
   dev-guide/index
   contributing
   changelog

Usage
=====

Read the :ref:`user-guide` to learn how to install, configure and use Furax
Poker.

Licence
=======

Furax Poker is made available under the GPL licence version 3. For more details,
see `LICENSE.txt <https://gitlab.com/etiennecrb/furax-poker/-/raw/master/LICENSE.txt>`_

Contributing
============

We happily welcome contributions, please see :ref:`contributing` for details.
To learn more about the internals of Furax Poker, please have a look at the
:ref:`dev-guide`.
