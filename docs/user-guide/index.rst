.. _user-guide:

==========
User guide
==========

.. toctree::
   :maxdepth: 2

   installation
   settings
   hud
