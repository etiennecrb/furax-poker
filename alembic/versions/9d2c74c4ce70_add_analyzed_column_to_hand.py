"""add analyzed column to hand

Revision ID: 9d2c74c4ce70
Revises: c0dbcd7ee1cf
Create Date: 2019-05-17 23:45:55.526965

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "9d2c74c4ce70"
down_revision = "c0dbcd7ee1cf"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("hand", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("analyzed", sa.Boolean(), server_default="FALSE", nullable=False)
        )


def downgrade():
    with op.batch_alter_table("hand", schema=None) as batch_op:
        batch_op.drop_column("analyzed")
