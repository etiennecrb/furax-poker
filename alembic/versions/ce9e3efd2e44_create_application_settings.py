"""create application settings

Revision ID: ce9e3efd2e44
Revises: 1b799404e311
Create Date: 2019-05-26 23:28:24.034198

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ce9e3efd2e44"
down_revision = "1b799404e311"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "applicationsettings",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("autostart_hud", sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.add_column(sa.Column("hero", sa.String(), nullable=True))


def downgrade():
    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.drop_column("hero")

    op.drop_table("applicationsettings")
