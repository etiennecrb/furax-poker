"""create history directory table

Revision ID: c0dbcd7ee1cf
Revises: 0b6c8abea4eb
Create Date: 2019-05-16 23:40:25.079872

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "c0dbcd7ee1cf"
down_revision = "0b6c8abea4eb"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "history",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("path", sa.String(), nullable=False),
        sa.Column("room", sa.Enum("WINAMAX", name="room"), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("path", "room"),
    )
    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_history_path"), ["path"], unique=False)
        batch_op.create_index(batch_op.f("ix_history_room"), ["room"], unique=False)


def downgrade():
    with op.batch_alter_table("history", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_history_room"))
        batch_op.drop_index(batch_op.f("ix_history_path"))

    op.drop_table("history")
