"""add favorite_seat setting

Revision ID: 5f3def1f50cc
Revises: 232874e3661c
Create Date: 2020-05-08 20:55:31.838633

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "5f3def1f50cc"
down_revision = "232874e3661c"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.add_column(sa.Column("favorite_seat", sa.Boolean()))
    op.execute("UPDATE applicationsettings SET favorite_seat = TRUE")
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.alter_column(
            "favorite_seat", existing_type=sa.Boolean(), nullable=False,
        )


def downgrade():
    with op.batch_alter_table("applicationsettings", schema=None) as batch_op:
        batch_op.drop_column("favorite_seat")
