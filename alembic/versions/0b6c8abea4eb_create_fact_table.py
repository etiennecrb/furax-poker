"""create fact table

Revision ID: 0b6c8abea4eb
Revises: 7c6783b52308
Create Date: 2019-05-15 00:00:00.696649

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0b6c8abea4eb"
down_revision = "7c6783b52308"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "fact",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column(
            "type",
            sa.Enum(
                "HAND_PLAYED",
                "PREFLOP_CHECK",
                "PREFLOP_CALL",
                "PREFLOP_BET",
                "PREFLOP_RAISE",
                "PREFLOP_FOLD",
                "FLOP_CHECK",
                "FLOP_CALL",
                "FLOP_BET",
                "FLOP_RAISE",
                "FLOP_FOLD",
                "TURN_CHECK",
                "TURN_CALL",
                "TURN_BET",
                "TURN_RAISE",
                "TURN_FOLD",
                "RIVER_CHECK",
                "RIVER_CALL",
                "RIVER_BET",
                "RIVER_RAISE",
                "RIVER_FOLD",
                "VPIP_CHANCE",
                "VPIP",
                "PFR",
                "CBET_CHANCE",
                "CBET",
                "FOLD_TO_CBET_CHANCE",
                "FOLD_TO_CBET",
                name="facttype",
            ),
            nullable=False,
        ),
        sa.Column("action_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["action_id"], ["action.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("action_id", "type"),
    )
    with op.batch_alter_table("fact", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_fact_action_id"), ["action_id"], unique=False
        )
        batch_op.create_index(batch_op.f("ix_fact_type"), ["type"], unique=False)


def downgrade():
    with op.batch_alter_table("fact", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_fact_type"))
        batch_op.drop_index(batch_op.f("ix_fact_action_id"))

    op.drop_table("fact")
