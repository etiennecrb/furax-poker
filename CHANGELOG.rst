Version 1.1.0
=============

Unreleased

New features
------------

- Allow changing statistics time scope: add setting to display either all time
  or current session statistics for opponent and hero.

Version 1.0.0
=============

Released 2020-05-17

New features
------------

- HUD with basic statistics (VPIP, PFR, AF, CBet, Fold to CBet)
- Hide HUD when window is not active
- HUD follows window position
- HUD handles favorite seat
- Build application for Windows, Debian and Fedora
- Set up documentation
