import datetime

from furax_poker.loader import load_hand
from furax_poker.hand import (
    ActionType,
    Board,
    GameType,
    Hand,
    Round,
    RoundType,
    Seat,
    SeatPosition,
    Table,
    Tournament,
    Room,
    Variant,
)


def test_load_hand(session):
    room = Room.WINAMAX
    raw_hand = {
        "hand_id": "810940995725164559-51-1489865625",
        "start_datetime": datetime.datetime(2017, 3, 18, 19, 33, 45),
        "variant": "NO_LIMIT_HOLDEM",
        "game_type": "TOURNAMENT",
        "table_name": "Hold'em [180 Max](188811914)#14",
        "tournament_name": "Hold'em [180 Max]",
        "tournament_id": "188811914",
        "buy_in": 2.0,
        "level": 6,
        "ante": 25,
        "small_blind": 150,
        "big_blind": 300,
        "max_seats": 6,
        "button_seat": 3,
        "hero_name": "TiennouFurax",
        "hero_cards": ["Ad", "2s"],
        "board": ["Ah", "6d", "6h", "Qc", "Qs"],
        "players": [
            {"name": "Le.Suisse", "seat": 1, "stack": 11700},
            {"name": "JmB0", "seat": 2, "stack": 13908},
            {"name": "pschitcitron", "seat": 3, "stack": 17197},
            {"name": "RoadToFish", "seat": 4, "stack": 14309},
            {"name": "TiennouFurax", "seat": 5, "stack": 18024},
            {"name": "gautron72", "seat": 6, "stack": 4162},
        ],
        "rounds": [
            {
                "actions": [
                    {
                        "player_name": "RoadToFish",
                        "type": "ANTE",
                        "value": 25,
                        "all_in": False,
                    },
                    {
                        "player_name": "TiennouFurax",
                        "type": "ANTE",
                        "value": 25,
                        "all_in": False,
                    },
                    {
                        "player_name": "Le.Suisse",
                        "type": "ANTE",
                        "value": 25,
                        "all_in": False,
                    },
                    {
                        "player_name": "JmB0",
                        "type": "ANTE",
                        "value": 25,
                        "all_in": False,
                    },
                    {
                        "player_name": "pschitcitron",
                        "type": "ANTE",
                        "value": 25,
                        "all_in": False,
                    },
                    {
                        "player_name": "gautron72",
                        "type": "ANTE",
                        "value": 25,
                        "all_in": False,
                    },
                    {
                        "player_name": "RoadToFish",
                        "type": "SMALL_BLIND",
                        "value": 150,
                        "all_in": False,
                    },
                    {
                        "player_name": "TiennouFurax",
                        "type": "BIG_BLIND",
                        "value": 300,
                        "all_in": False,
                    },
                ]
            },
            {
                "actions": [
                    {
                        "player_name": "gautron72",
                        "type": "FOLD",
                        "value": None,
                        "all_in": False,
                    },
                    {
                        "player_name": "Le.Suisse",
                        "type": "CALL",
                        "value": 300,
                        "all_in": False,
                    },
                    {
                        "player_name": "JmB0",
                        "type": "FOLD",
                        "value": None,
                        "all_in": False,
                    },
                    {
                        "player_name": "pschitcitron",
                        "type": "FOLD",
                        "value": None,
                        "all_in": False,
                    },
                    {
                        "player_name": "RoadToFish",
                        "type": "FOLD",
                        "value": None,
                        "all_in": False,
                    },
                    {
                        "player_name": "TiennouFurax",
                        "type": "CHECK",
                        "value": None,
                        "all_in": False,
                    },
                ]
            },
            {
                "actions": [
                    {
                        "player_name": "TiennouFurax",
                        "type": "CHECK",
                        "value": None,
                        "all_in": False,
                    },
                    {
                        "player_name": "Le.Suisse",
                        "type": "BET",
                        "value": 450,
                        "all_in": False,
                    },
                    {
                        "player_name": "TiennouFurax",
                        "type": "CALL",
                        "value": 450,
                        "all_in": False,
                    },
                ]
            },
            {
                "actions": [
                    {
                        "player_name": "TiennouFurax",
                        "type": "CHECK",
                        "value": None,
                        "all_in": False,
                    },
                    {
                        "player_name": "Le.Suisse",
                        "type": "CHECK",
                        "value": None,
                        "all_in": False,
                    },
                ]
            },
            {
                "actions": [
                    {
                        "player_name": "TiennouFurax",
                        "type": "BET",
                        "value": 1200,
                        "all_in": False,
                    },
                    {
                        "player_name": "Le.Suisse",
                        "type": "CALL",
                        "value": 1200,
                        "all_in": False,
                    },
                ]
            },
            {
                "actions": [
                    {
                        "player_name": "Le.Suisse",
                        "type": "SHOW",
                        "value": ["3c", "As"],
                        "all_in": False,
                    },
                    {
                        "player_name": "TiennouFurax",
                        "type": "SHOW",
                        "value": ["Ad", "2s"],
                        "all_in": False,
                    },
                    {
                        "player_name": "TiennouFurax",
                        "type": "COLLECT",
                        "value": 2100,
                        "all_in": False,
                    },
                    {
                        "player_name": "Le.Suisse",
                        "type": "COLLECT",
                        "value": 2100,
                        "all_in": False,
                    },
                ]
            },
        ],
    }

    load_hand(session, room, raw_hand)
    hand = session.query(Hand).one()
    assert hand.key == "810940995725164559-51-1489865625"
    assert hand.start_datetime == datetime.datetime(2017, 3, 18, 19, 33, 45)
    assert hand.variant is Variant.NO_LIMIT_HOLDEM
    assert hand.game_type is GameType.TOURNAMENT
    assert hand.buy_in == 2.0
    assert hand.level == 6
    assert hand.ante == 25
    assert hand.small_blind == 150
    assert hand.big_blind == 300
    assert hand.max_seats == 6

    table = session.query(Table).one()
    assert hand.table is table
    assert table.room is room
    assert table.name == "Hold'em [180 Max](188811914)#14"

    tournament = session.query(Tournament).one()
    assert table.tournament is tournament
    assert tournament.room is room
    assert tournament.key == "188811914"
    assert tournament.name == "Hold'em [180 Max]"

    board = session.query(Board).one()
    assert hand.board is board
    assert board.flop_1 == "Ah"
    assert board.flop_2 == "6d"
    assert board.flop_3 == "6h"
    assert board.turn == "Qc"
    assert board.river == "Qs"

    seat_props_by_player_name = {
        "Le.Suisse": {
            "number": 1,
            "stack": 11700,
            "position": SeatPosition.UTG_2,
            "order": SeatPosition.UTG_2.order,
            "is_hero": False,
            "card_1": "3c",
            "card_2": "As",
        },
        "JmB0": {
            "number": 2,
            "stack": 13908,
            "position": SeatPosition.CUTOFF,
            "order": SeatPosition.CUTOFF.order,
            "is_hero": False,
            "card_1": None,
            "card_2": None,
        },
        "pschitcitron": {
            "number": 3,
            "stack": 17197,
            "position": SeatPosition.BUTTON,
            "order": SeatPosition.BUTTON.order,
            "is_hero": False,
            "card_1": None,
            "card_2": None,
        },
        "RoadToFish": {
            "number": 4,
            "stack": 14309,
            "position": SeatPosition.SMALL_BLIND,
            "order": SeatPosition.SMALL_BLIND.order,
            "is_hero": False,
            "card_1": None,
            "card_2": None,
        },
        "TiennouFurax": {
            "number": 5,
            "stack": 18024,
            "position": SeatPosition.BIG_BLIND,
            "order": SeatPosition.BIG_BLIND.order,
            "is_hero": True,
            "card_1": "Ad",
            "card_2": "2s",
        },
        "gautron72": {
            "number": 6,
            "stack": 4162,
            "position": SeatPosition.UTG_1,
            "order": SeatPosition.UTG_1.order,
            "is_hero": False,
            "card_1": None,
            "card_2": None,
        },
    }
    seat_by_player_name = {seat.player.name: seat for seat in session.query(Seat).all()}
    assert len(seat_by_player_name) == 6
    assert len(hand.seats) == 6
    for seat in hand.seats:
        assert seat.player.room == room
        expected_props = seat_props_by_player_name[seat.player.name]
        for attr, value in expected_props.items():
            assert getattr(seat, attr) == value

    rounds = session.query(Round).all()
    assert len(rounds) == 6
    assert len(hand.rounds) == 6
    assert hand.rounds[0].type is RoundType.BLINDS
    assert hand.rounds[0].order is RoundType.BLINDS.order
    assert hand.rounds[1].type is RoundType.PREFLOP
    assert hand.rounds[1].order is RoundType.PREFLOP.order
    assert hand.rounds[2].type is RoundType.FLOP
    assert hand.rounds[2].order is RoundType.FLOP.order
    assert hand.rounds[3].type is RoundType.TURN
    assert hand.rounds[3].order is RoundType.TURN.order
    assert hand.rounds[4].type is RoundType.RIVER
    assert hand.rounds[4].order is RoundType.RIVER.order
    assert hand.rounds[5].type is RoundType.SHOWDOWN
    assert hand.rounds[5].order is RoundType.SHOWDOWN.order

    expected_actions = [
        [
            {
                "player_name": "RoadToFish",
                "order": 0,
                "type": ActionType.ANTE,
                "value": 25,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "order": 1,
                "type": ActionType.ANTE,
                "value": 25,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "order": 2,
                "type": ActionType.ANTE,
                "value": 25,
                "all_in": False,
            },
            {
                "player_name": "JmB0",
                "order": 3,
                "type": ActionType.ANTE,
                "value": 25,
                "all_in": False,
            },
            {
                "player_name": "pschitcitron",
                "order": 4,
                "type": ActionType.ANTE,
                "value": 25,
                "all_in": False,
            },
            {
                "player_name": "gautron72",
                "order": 5,
                "type": ActionType.ANTE,
                "value": 25,
                "all_in": False,
            },
            {
                "player_name": "RoadToFish",
                "order": 6,
                "type": ActionType.SMALL_BLIND,
                "value": 150,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "order": 7,
                "type": ActionType.BIG_BLIND,
                "value": 300,
                "all_in": False,
            },
        ],
        [
            {
                "player_name": "gautron72",
                "order": 0,
                "type": ActionType.FOLD,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "order": 1,
                "type": ActionType.CALL,
                "value": 300,
                "all_in": False,
            },
            {
                "player_name": "JmB0",
                "order": 2,
                "type": ActionType.FOLD,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "pschitcitron",
                "order": 3,
                "type": ActionType.FOLD,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "RoadToFish",
                "order": 4,
                "type": ActionType.FOLD,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "order": 5,
                "type": ActionType.CHECK,
                "value": None,
                "all_in": False,
            },
        ],
        [
            {
                "player_name": "TiennouFurax",
                "order": 0,
                "type": ActionType.CHECK,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "order": 1,
                "type": ActionType.BET,
                "value": 450,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "order": 2,
                "type": ActionType.CALL,
                "value": 450,
                "all_in": False,
            },
        ],
        [
            {
                "player_name": "TiennouFurax",
                "order": 0,
                "type": ActionType.CHECK,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "order": 1,
                "type": ActionType.CHECK,
                "value": None,
                "all_in": False,
            },
        ],
        [
            {
                "player_name": "TiennouFurax",
                "order": 0,
                "type": ActionType.BET,
                "value": 1200,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "order": 1,
                "type": ActionType.CALL,
                "value": 1200,
                "all_in": False,
            },
        ],
        [
            {
                "player_name": "Le.Suisse",
                "order": 0,
                "type": ActionType.SHOW,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "order": 1,
                "type": ActionType.SHOW,
                "value": None,
                "all_in": False,
            },
            {
                "player_name": "TiennouFurax",
                "order": 2,
                "type": ActionType.COLLECT,
                "value": 2100,
                "all_in": False,
            },
            {
                "player_name": "Le.Suisse",
                "order": 3,
                "type": ActionType.COLLECT,
                "value": 2100,
                "all_in": False,
            },
        ],
    ]

    for i, round in enumerate(hand.rounds):
        assert len(round.actions) == len(expected_actions[i])
        for j, action in enumerate(round.actions):
            expected_action = expected_actions[i][j]
            expected_seat = seat_by_player_name[expected_action["player_name"]]
            assert action.order == expected_action["order"]
            assert action.type == expected_action["type"]
            assert action.value == expected_action["value"]
            assert action.all_in == expected_action["all_in"]
            assert action.seat is expected_seat


def test_load_hand_without_hero(session):
    """Test that hand without hero name can be loaded."""
    room = Room.WINAMAX

    raw_hand = {
        "hand_id": "810940995725164559-51-1489865625",
        "start_datetime": datetime.datetime(2017, 3, 18, 19, 33, 45),
        "variant": "NO_LIMIT_HOLDEM",
        "game_type": "TOURNAMENT",
        "table_name": "Hold'em [180 Max](188811914)#14",
        "tournament_name": "Hold'em [180 Max]",
        "tournament_id": "188811914",
        "buy_in": 2.0,
        "level": 6,
        "ante": 25,
        "small_blind": 150,
        "big_blind": 300,
        "max_seats": 6,
        "button_seat": 3,
        "board": ["Ah", "6d", "6h", "Qc", "Qs"],
        "players": [
            {"name": "Le.Suisse", "seat": 1, "stack": 11700},
            {"name": "JmB0", "seat": 2, "stack": 13908},
            {"name": "pschitcitron", "seat": 3, "stack": 17197},
            {"name": "RoadToFish", "seat": 4, "stack": 14309},
            {"name": "TiennouFurax", "seat": 5, "stack": 18024},
            {"name": "gautron72", "seat": 6, "stack": 4162},
        ],
        "rounds": [{"actions": []}],
    }

    load_hand(session, room, raw_hand)
    hands = session.query(Hand).all()
    assert len(hands) == 1
