from unittest.mock import Mock

import pytest

from furax_poker.gui.sync_indicator import SyncIndicator
from furax_poker.tracker import Tracker


@pytest.fixture
def sync_indicator(event_loop):
    app = Mock()
    app.tracker = Tracker()
    return SyncIndicator(app)


def test_hidden_by_default(sync_indicator):
    assert sync_indicator.isVisible() is False


def test_synchronization_progress(sync_indicator):
    sync_indicator.app.tracker.load_progress.emit(0.5)
    assert sync_indicator.isVisible() is True
    assert sync_indicator._progressBar.value() == 35

    sync_indicator.app.tracker.analyze_progress.emit(0.5)
    assert sync_indicator.isVisible() is True
    assert sync_indicator._progressBar.value() == 85

    sync_indicator.app.tracker.sync_finished.emit()
    assert sync_indicator.isVisible() is False
