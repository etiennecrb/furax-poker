from unittest.mock import Mock

from PyQt5.QtCore import QEvent, QPoint, QPointF, Qt
from PyQt5.QtGui import QMouseEvent
import pytest

from furax_poker.fact import FactMetrics
from furax_poker.gui.hud_window import HudWindow
from furax_poker.hand import Player


@pytest.fixture
def hud_window(event_loop):
    hud = Mock()
    return HudWindow(hud)


def press_event(x: int = 0, y: int = 0):
    return QMouseEvent(
        QEvent.MouseButtonPress,
        QPointF(x, y),
        Qt.LeftButton,
        Qt.LeftButton,
        Qt.NoModifier,
    )


def release_event():
    return QMouseEvent(
        QEvent.MouseButtonRelease,
        QPointF(),
        Qt.LeftButton,
        Qt.LeftButton,
        Qt.NoModifier,
    )


def move_event(x: int = 0, y: int = 0):
    return QMouseEvent(
        QEvent.MouseMove,
        QPointF(),
        QPointF(x, y),
        Qt.NoButton,
        Qt.NoButton,
        Qt.NoModifier,
    )


def test_create(hud_window):
    assert bool(hud_window)


def test_change_view_on_click(hud_window):
    assert hud_window._layout.currentIndex() == 0
    hud_window.mousePressEvent(press_event())
    hud_window.mouseReleaseEvent(release_event())
    assert hud_window._layout.currentIndex() == 1
    hud_window.mousePressEvent(press_event())
    hud_window.mouseReleaseEvent(release_event())
    assert hud_window._layout.currentIndex() == 0


def test_dont_change_view_on_drag(hud_window):
    assert hud_window._layout.currentIndex() == 0
    hud_window.mousePressEvent(press_event())
    hud_window.mouseMoveEvent(move_event())
    hud_window.mouseReleaseEvent(release_event())
    assert hud_window._layout.currentIndex() == 0


def test_drag_window(hud_window, detect_changes):
    assert hud_window.pos() == QPoint(0, 0)
    hud_window.mousePressEvent(press_event(6, 12))
    hud_window.mouseMoveEvent(move_event(10, 25))
    hud_window.mouseReleaseEvent(release_event())
    assert hud_window.pos() == QPoint(4, 13)

    hud_window.mousePressEvent(press_event(10, 5))
    hud_window.mouseMoveEvent(move_event(30, 10))
    hud_window.mouseReleaseEvent(release_event())
    assert hud_window.pos() == QPoint(20, 5)


def test_update(hud_window, room):
    seat = Mock()
    seat.player = Player(room=room, name="Player 1")
    metrics = FactMetrics(hand_played=1000, vpip=2, vpip_chance=8, cbet=2)
    hud_window.refresh(seat, metrics)
    assert hud_window._mainView._playerLabel.text() == "Player 1"
    assert hud_window._mainView._handPlayedLabel.text() == "1K"
    assert hud_window._mainView._vpipLabel.text() == "20%"
    assert hud_window._mainView._pfrLabel.text() == "0%"
    assert hud_window._mainView._afLabel.text() == "-"
    assert hud_window._secondaryView._playerLabel.text() == "Player 1"
    assert hud_window._secondaryView._handPlayedLabel.text() == "1K"
    assert hud_window._secondaryView._cbetLabel.text() == "100% (2)"
    assert hud_window._secondaryView._foldToCbetLabel.text() == "-"
