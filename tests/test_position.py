import pytest

from furax_poker.active_window import ActiveWindow
from furax_poker.hand import SeatPosition
from furax_poker.position import PositionStrategy


@pytest.fixture
def configure(hand_builder, hud_window_factory):
    def _configure(position_strategy, seating_plan):
        """Fixture mocking behaviour of a HUD controller using given position strategy.

        Given a position strategy and a seating plan, this function will create a fake
        hand and mimick HUD controller behaviour receiving this new hand: it will create
        required HUD windows or reuse existing ones, attach them to the strategy and
        refresh strategy. It returns the list of currently opened windows sorted by seat
        number.
        """
        hand = hand_builder.create(seating_plan=seating_plan)
        windows_to_detach = set(hud_window_factory.windows)
        for seat in hand.seats:
            try:
                window = hud_window_factory.get(seat.number)
                windows_to_detach.remove(window)
            except StopIteration:
                window = hud_window_factory()
                window.seat = seat
                position_strategy.attach(window)
        for window in windows_to_detach:
            window.close()
            position_strategy.detach(window)
        position_strategy.refresh(hand)
        windows = hud_window_factory.windows
        assert len(windows) == len(hand.seats)
        return sorted(windows, key=lambda w: w.seat.number)

    return _configure


def mock_user_position(*windows):
    """Move given windows to predefined positions."""
    pos = [(15, 20), (40, 15), (80, 50), (30, 20)]
    for i, window in enumerate(windows):
        window.x, window.y = pos[i]


@pytest.mark.parametrize("favorite_seat", [True, False])
@pytest.mark.parametrize("hide_hud_when_window_not_active", [True, False])
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_position_window_reopened(
    favorite_seat, hide_hud_when_window_not_active, follow_active_window, configure
):
    """Test that strategy remembers position of closed windows to reopen same seat number
    at same place.
    """
    seating_plan = [
        (1, "Player 1"),
        (2, "Player 2"),
        (3, "Hero", SeatPosition.BUTTON),
    ]
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    window_1, window_2, window_3 = configure(
        position_strategy, seating_plan=seating_plan,
    )
    position_strategy.apply(None)

    # Simulate user positionning its windows
    mock_user_position(window_1, window_2, window_3)

    # New hand with same seating plan
    configure(position_strategy, seating_plan)
    position_strategy.apply(None)

    # Strategy didn't do anything but now remember user positioning
    assert (window_1.x, window_1.y) == (15, 20)
    assert (window_2.x, window_2.y) == (40, 15)
    assert (window_3.x, window_3.y) == (80, 50)

    # New hand without seat 2
    configure(position_strategy, [(1, "Player 1"), (3, "Hero", SeatPosition.BUTTON)])
    position_strategy.apply(None)

    # New player on seat 2
    window_1, new_window_2, window_3 = configure(
        position_strategy,
        [(1, "Player 1"), (2, "New player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )
    position_strategy.apply(None)

    # Strategy applied previous position on new window 2
    assert (new_window_2.x, new_window_2.y) == (40, 15)


@pytest.mark.parametrize("favorite_seat", [True, False])
@pytest.mark.parametrize("hide_hud_when_window_not_active", [True, False])
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_clear_cache_when_seating_plan_changes(
    favorite_seat, hide_hud_when_window_not_active, follow_active_window, configure
):
    """Test that strategy clear cache of closed windows positions when seating plan changed,
    in order not to reopen same seat at previous place because it won't be relevant
    anymore.
    """
    seating_plan = [
        (1, "Player 1"),
        (2, "Player 2"),
        (3, "Hero", SeatPosition.BUTTON),
    ]
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    window_1, window_2, window_3 = configure(
        position_strategy, seating_plan=seating_plan,
    )
    position_strategy.apply(None)

    # Simulate user positionning its windows
    mock_user_position(window_1, window_2, window_3)

    # New hand with same seating plan
    configure(position_strategy, seating_plan)
    position_strategy.apply(None)

    # Strategy didn't do anything but now remember user positioning
    assert (window_1.x, window_1.y) == (15, 20)
    assert (window_2.x, window_2.y) == (40, 15)
    assert (window_3.x, window_3.y) == (80, 50)

    # New hand without seat 2 and change in seating plan (hero changed seat)
    configure(position_strategy, [(1, "Hero"), (3, "Player 1", SeatPosition.BUTTON)])
    position_strategy.apply(None)

    # New player on seat 2
    window_1, new_window_2, window_3 = configure(
        position_strategy,
        [(1, "Hero"), (2, "New player 2"), (3, "Player 1", SeatPosition.BUTTON)],
    )
    position_strategy.apply(None)

    # Strategy didn't apply previous position on new window 2
    assert (new_window_2.x, new_window_2.y) == (0, 0)


@pytest.mark.parametrize(
    "favorite_seat,expected_positions",
    [(True, [(80, 50), (15, 20), (40, 15)]), (False, [(15, 20), (40, 15), (80, 50)])],
)
@pytest.mark.parametrize("hide_hud_when_window_not_active", [True, False])
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_position_with_table_change(
    favorite_seat,
    expected_positions,
    hide_hud_when_window_not_active,
    follow_active_window,
    configure,
):
    """Test that strategy handles hero changing seat."""
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    windows = configure(
        position_strategy,
        [(1, "Player 1"), (2, "Player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )
    position_strategy.apply(None)

    # Simulate user positionning its windows
    mock_user_position(*windows)

    # New hand with same seating plan
    configure(
        position_strategy,
        [(1, "Player 1"), (2, "Player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )
    position_strategy.apply(None)

    # New hand after table change
    windows = configure(
        position_strategy,
        [(1, "Hero"), (2, "Player 1"), (3, "Player 2", SeatPosition.BUTTON)],
    )
    position_strategy.apply(None)

    for i, window in enumerate(windows):
        assert (window.x, window.y) == expected_positions[i]


@pytest.mark.parametrize(
    "favorite_seat,expected_positions",
    [
        (
            True,
            # Strategy kept hero at the same place so window_1 has now window_5
            # position. However window 2 has lost its position since it now maps to
            # an unexisting window, it's important to reset its position otherwise
            # there will be overlapping windows.
            [(30, 20), (0, 0), (15, 20), (40, 15), (80, 50), (0, 0)],
        ),
        (False, [(15, 20), (40, 15), (80, 50), (0, 0), (30, 20), (0, 0)]),
    ],
)
@pytest.mark.parametrize("hide_hud_when_window_not_active", [True, False])
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_position_with_table_change_more_seats(
    favorite_seat,
    expected_positions,
    hide_hud_when_window_not_active,
    follow_active_window,
    configure,
):
    """Test that strategy handles hero changing seat when new table has more seats."""
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    windows = configure(
        position_strategy,
        [
            (1, "Player 1"),
            (2, "Player 2"),
            (3, "Player 3"),
            (5, "Hero", SeatPosition.BUTTON),
        ],
    )
    position_strategy.apply(None)

    # Simulate user positionning its windows
    mock_user_position(*windows)

    # New hand after table change
    windows = configure(
        position_strategy,
        [
            (1, "Hero"),
            (2, "Player 4"),
            (3, "Player 1"),
            (4, "Player 3"),
            (5, "Player 2", SeatPosition.BUTTON),
            (6, "Player 5"),
        ],
    )
    position_strategy.apply(None)

    for i, window in enumerate(windows):
        assert (window.x, window.y) == expected_positions[i]


@pytest.mark.parametrize("favorite_seat", [True, False])
@pytest.mark.parametrize("hide_hud_when_window_not_active", [True, False])
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_default_position(
    favorite_seat, hide_hud_when_window_not_active, follow_active_window, configure
):
    """Test that strategy positions windows at top left of screen by default."""
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    windows = configure(
        position_strategy,
        [(1, "Player 1"), (2, "Player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )

    expected_x, expected_y = 0, 0
    position_strategy.apply(None)

    for window in windows:
        assert (window.x, window.y) == (expected_x, expected_y)


@pytest.mark.parametrize("favorite_seat", [True, False])
@pytest.mark.parametrize("hide_hud_when_window_not_active", [True, False])
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_default_position_center_of_active_window(
    favorite_seat, hide_hud_when_window_not_active, follow_active_window, configure
):
    """Test that strategy positions windows at center of active window if available."""
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    windows = configure(
        position_strategy,
        [(1, "Player 1"), (2, "Player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )

    active_window = ActiveWindow(
        id=1, title=windows[0].seat.hand.table.name, x=10, y=20, width=500, height=300
    )
    expected_x, expected_y = 260, 170
    position_strategy.apply(active_window)

    for window in windows:
        assert (window.x, window.y) == (expected_x, expected_y)


@pytest.mark.parametrize("favorite_seat", [True, False])
@pytest.mark.parametrize(
    "hide_hud_when_window_not_active,active_window,expected_is_visible",
    [
        (True, None, True),
        (True, ActiveWindow(id=1, title="Table"), True),
        (True, ActiveWindow(id=1, title="Firefox"), False),
        (False, None, True),
        (False, ActiveWindow(id=1, title="Table"), True),
        (False, ActiveWindow(id=1, title="Firefox"), True),
    ],
)
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_first_visibility(
    favorite_seat,
    hide_hud_when_window_not_active,
    follow_active_window,
    active_window,
    expected_is_visible,
    configure,
):
    """Test that strategy makes windows visible at first if active window is None, or if
    defined hide or show window depending on hide_hud_when_window_not_active setting.
    """
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    windows = configure(
        position_strategy,
        [(1, "Player 1"), (2, "Player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )
    position_strategy.apply(active_window)
    for window in windows:
        assert window.is_visible == expected_is_visible


@pytest.mark.parametrize("favorite_seat", [True, False])
@pytest.mark.parametrize(
    (
        "hide_hud_when_window_not_active,"
        "active_window_1,active_window_2,expected_is_visible"
    ),
    [
        (True, None, None, True),
        (True, None, ActiveWindow(id=1, title="Table"), True),
        (True, None, ActiveWindow(id=1, title="Firefox"), False),
        (True, ActiveWindow(id=1, title="Table"), None, True),
        (
            True,
            ActiveWindow(id=1, title="Table"),
            ActiveWindow(id=1, title="Table"),
            True,
        ),
        (
            True,
            ActiveWindow(id=1, title="Table"),
            ActiveWindow(id=1, title="Firefox"),
            False,
        ),
        (True, ActiveWindow(id=1, title="Firefox"), None, False),
        (
            True,
            ActiveWindow(id=1, title="Firefox"),
            ActiveWindow(id=1, title="Table"),
            True,
        ),
        (
            True,
            ActiveWindow(id=1, title="Firefox"),
            ActiveWindow(id=1, title="Firefox"),
            False,
        ),
        (False, None, None, True),
        (False, None, ActiveWindow(id=1, title="Table"), True),
        (False, None, ActiveWindow(id=1, title="Firefox"), True),
        (False, ActiveWindow(id=1, title="Table"), None, True),
        (
            False,
            ActiveWindow(id=1, title="Table"),
            ActiveWindow(id=1, title="Table"),
            True,
        ),
        (
            False,
            ActiveWindow(id=1, title="Table"),
            ActiveWindow(id=1, title="Firefox"),
            True,
        ),
        (False, ActiveWindow(id=1, title="Firefox"), None, True),
        (
            False,
            ActiveWindow(id=1, title="Firefox"),
            ActiveWindow(id=1, title="Table"),
            True,
        ),
        (
            False,
            ActiveWindow(id=1, title="Firefox"),
            ActiveWindow(id=1, title="Firefox"),
            True,
        ),
    ],
)
@pytest.mark.parametrize("follow_active_window", [True, False])
def test_active_window_changes(
    favorite_seat,
    hide_hud_when_window_not_active,
    follow_active_window,
    active_window_1,
    active_window_2,
    expected_is_visible,
    configure,
):
    """Test that strategy makes windows visible at first if active window is None."""
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    windows = configure(
        position_strategy,
        [(1, "Player 1"), (2, "Player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )
    position_strategy.apply(active_window_1)
    position_strategy.apply(active_window_2)
    for window in windows:
        assert window.is_visible == expected_is_visible


@pytest.mark.parametrize("favorite_seat", [True, False])
@pytest.mark.parametrize("hide_hud_when_window_not_active", [True, False])
@pytest.mark.parametrize(
    "follow_active_window,delta_x,delta_y,delta_width,delta_height,expected_positions",
    [
        (True, 20, 40, 0, 0, [(35, 60), (60, 55), (100, 90)]),
        (True, 0, 0, 20, 25, [(19, 25), (50, 19), (100, 62)]),
        (True, 20, 40, 20, 25, [(39, 65), (70, 59), (120, 102)]),
        (False, 20, 40, 0, 0, [(15, 20), (40, 15), (80, 50)]),
        (False, 0, 0, 20, 25, [(15, 20), (40, 15), (80, 50)]),
        (False, 20, 40, 20, 25, [(15, 20), (40, 15), (80, 50)]),
    ],
)
def test_active_window_moves(
    favorite_seat,
    hide_hud_when_window_not_active,
    follow_active_window,
    delta_x,
    delta_y,
    delta_width,
    delta_height,
    expected_positions,
    configure,
):
    """Test that strategy makes windows follow active window."""
    position_strategy = PositionStrategy(
        favorite_seat=favorite_seat,
        hide_hud_when_window_not_active=hide_hud_when_window_not_active,
        follow_active_window=follow_active_window,
    )
    active_window = ActiveWindow(id=1, title="Table", x=0, y=0, width=100, height=100)
    windows = configure(
        position_strategy,
        [(1, "Player 1"), (2, "Player 2"), (3, "Hero", SeatPosition.BUTTON)],
    )
    position_strategy.apply(active_window)
    mock_user_position(*windows)
    position_strategy.apply(active_window)

    # Simulate user going on another window to test it is not used
    another_active_window = ActiveWindow(
        id=2, title="Firefox", x=30, y=50, width=300, height=800
    )
    position_strategy.apply(another_active_window)

    # Move active window
    position_strategy.apply(active_window)
    active_window = ActiveWindow(
        id=1,
        title="Table",
        x=active_window.x + delta_x,
        y=active_window.y + delta_y,
        width=active_window.width + delta_width,
        height=active_window.height + delta_height,
    )
    position_strategy.apply(active_window)

    for i, window in enumerate(windows):
        assert (window.x, window.y) == expected_positions[i]
