Winamax Poker - Tournament "Hold\'em [180 Max]" buyIn: 1.80€ + 0.20€ level: 6 - HandId: #810940995725164559-51-1489865625 - Holdem no limit (25/150/300) - 2017/03/18 19:33:45 UTC
Table: 'Hold'em [180 Max](188811914)#14' 6-max (real money) Seat #3 is the button
Seat 1: Le.Suisse (11700)
Seat 2: JmB0 (13908)
Seat 3: pschitcitron (17197)
Seat 4: RoadToFish (14309)
Seat 5: TiennouFurax (18024)
Seat 6: gautron72 (4162)
*** ANTE/BLINDS ***
RoadToFish posts small blind 150
TiennouFurax posts big blind 300
Dealt to TiennouFurax [Ad 2s]
*** PRE-FLOP ***
gautron72 folds
Le.Suisse folds
JmB0 folds
pschitcitron folds
RoadToFish folds
TiennouFurax folds
*** SUMMARY ***
Total pot 4200 | No rake
Board: [Ah 6d 6h Qc Qs]


