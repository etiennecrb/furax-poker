from pathlib import Path
from unittest.mock import call, Mock

from furax_poker.fact import Fact
from furax_poker.hand import Hand, Table
from furax_poker.tracker import (
    Tracker,
    ObserverThread,
    SynchronizerThread,
    is_history_file,
)
from furax_poker.settings import History, HistoryFile


def test_tracker_refresh(history, detect_changes):
    f = Path(history.path) / "tournament.txt"

    refreshed_callback = Mock()

    tracker = Tracker()
    tracker.refreshed.connect(refreshed_callback)
    tracker.start()

    detect_changes()
    f.touch()
    detect_changes()

    refreshed_callback.assert_called_once_with()


def test_tracker_stop_refresh(history, detect_changes):
    f = Path(history.path) / "tournament.txt"

    refreshed_callback = Mock()

    tracker = Tracker()
    tracker.refreshed.connect(refreshed_callback)
    tracker.start()
    detect_changes()
    tracker.stop()

    f.touch()
    detect_changes()
    refreshed_callback.assert_not_called()


def test_tracker_restart_history_changed(session, room, tmp_path, detect_changes):
    path_1 = tmp_path / "1"
    path_1.mkdir()
    path_2 = tmp_path / "2"
    path_2.mkdir()

    history = History(path=str(path_1), room=room)
    session.add(history)
    session.commit()

    f = Path(history.path) / "tournament.txt"
    refreshed_callback = Mock()

    tracker = Tracker()
    tracker.refreshed.connect(refreshed_callback)
    tracker.start()
    detect_changes()

    f.touch()
    detect_changes()

    refreshed_callback.assert_called_once_with()

    history.path = str(path_2)
    session.commit()

    tracker.restart()
    detect_changes()

    f.touch()
    detect_changes()

    refreshed_callback.assert_called_once_with()

    f = Path(history.path) / "tournament.txt"
    f.touch()
    detect_changes()

    assert len(refreshed_callback.mock_calls) == 2


def test_tracker_table_joined(session, history, raw_hand, detect_changes):
    table_joined_callback = Mock()
    hand_loaded_callback = Mock()

    tracker = Tracker()
    tracker.table_joined.connect(table_joined_callback)
    tracker.hand_loaded.connect(hand_loaded_callback)
    tracker.start()
    detect_changes()

    f = Path(history.path) / "tournament.txt"
    f.write_text(raw_hand("hand_1"))
    detect_changes()

    assert len(table_joined_callback.mock_calls) == 1
    (table,) = table_joined_callback.mock_calls[0].args
    assert table.id == session.query(Table).one().id

    assert len(hand_loaded_callback.mock_calls) == 1
    (hand,) = hand_loaded_callback.mock_calls[0].args
    assert hand.id == session.query(Hand).one().id


def test_tracker_table_joined_same_tournament(
    session, history, raw_hand, detect_changes
):
    table_joined_callback = Mock()
    hand_loaded_callback = Mock()

    tracker = Tracker()
    tracker.table_joined.connect(table_joined_callback)
    tracker.hand_loaded.connect(hand_loaded_callback)
    tracker.start()
    detect_changes()

    f = Path(history.path) / "tournament.txt"
    f.write_text(raw_hand("hand_1"))
    detect_changes()

    assert len(table_joined_callback.mock_calls) == 1
    assert len(hand_loaded_callback.mock_calls) == 1
    (hand_1,) = hand_loaded_callback.mock_calls[0].args

    f.write_text(raw_hand("hand_2"))
    detect_changes()

    assert len(table_joined_callback.mock_calls) == 1
    (table,) = table_joined_callback.mock_calls[0].args
    assert table.id == hand_1.table_id

    assert len(hand_loaded_callback.mock_calls) == 2
    (hand_2,) = hand_loaded_callback.mock_calls[1].args
    assert len(tracker.tables) == 1
    (table,) = tracker.tables
    assert table.id == hand_2.id


def test_tracker_restart_history_removed(session, room, tmp_path, detect_changes):
    path_1 = tmp_path / "1"
    path_1.mkdir()
    path_2 = tmp_path / "2"
    path_2.mkdir()

    history = History(path=str(path_1), room=room)
    session.add(history)
    session.commit()

    f = Path(history.path) / "tournament.txt"
    callback = Mock()

    tracker = Tracker()
    tracker.refreshed.connect(callback)
    tracker.start()
    detect_changes()

    f.touch()
    detect_changes()

    callback.assert_called_once_with()

    session.delete(history)
    history = History(path=str(path_2), room=room)
    session.add(history)
    session.commit()

    tracker.restart()
    detect_changes()

    f.touch()
    detect_changes()

    callback.assert_called_once_with()

    f = Path(history.path) / "tournament.txt"
    f.touch()
    detect_changes()

    assert len(callback.mock_calls) == 2


def test_synchronizer_no_file(history, detect_changes):
    synchronizer_thread = SynchronizerThread(history)

    sync_started_callback = Mock()
    sync_finished_callback = Mock()
    synchronizer_thread.sync_started.connect(sync_started_callback)
    synchronizer_thread.sync_finished.connect(sync_finished_callback)

    synchronizer_thread.start()
    detect_changes()
    synchronizer_thread.join()

    sync_started_callback.assert_called_once_with()
    sync_finished_callback.assert_called_once_with()


def test_synchronizer_new_file(session, history, raw_hand, caplog, detect_changes):
    f = Path(history.path) / "tournament.txt"
    f.write_text(raw_hand("hand_1"))

    synchronizer_thread = SynchronizerThread(history)

    sync_started_callback = Mock()
    load_progress_callback = Mock()
    analyze_progress_callback = Mock()
    sync_finished_callback = Mock()
    synchronizer_thread.sync_started.connect(sync_started_callback)
    synchronizer_thread.load_progress.connect(load_progress_callback)
    synchronizer_thread.analyze_progress.connect(analyze_progress_callback)
    synchronizer_thread.sync_finished.connect(sync_finished_callback)

    synchronizer_thread.start()
    detect_changes()
    synchronizer_thread.join()

    hand = session.query(Hand).one()
    assert hand.analyzed is True
    assert len(session.query(Fact).all()) >= 1
    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert round(history_file.last_sync_datetime.timestamp()) == round(
        f.stat().st_mtime
    )
    assert (
        len([record for record in caplog.records if record.levelname == "ERROR"]) == 0
    )

    sync_started_callback.assert_called_once_with()
    load_progress_calls = load_progress_callback.mock_calls
    assert len(load_progress_calls) == 2
    assert load_progress_calls[0] == call(0)
    assert load_progress_calls[1] == call(1)
    analyze_progress_calls = analyze_progress_callback.mock_calls
    assert len(analyze_progress_calls) == 2
    assert analyze_progress_calls[0] == call(0)
    assert analyze_progress_calls[1] == call(1)
    sync_finished_callback.assert_called_once_with()


def test_synchronizer_existing_file(session, history, raw_hand, caplog, detect_changes):
    f = Path(history.path) / "tournament.txt"
    f.write_text(raw_hand("hand_1"))

    synchronizer_thread = SynchronizerThread(history)
    synchronizer_thread.start()
    detect_changes()
    synchronizer_thread.join()
    assert len(session.query(Hand).all()) == 1

    # Reload history
    session.expire(history)
    history = session.query(History).one()

    synchronizer_thread = SynchronizerThread(history)

    sync_started_callback = Mock()
    load_progress_callback = Mock()
    analyze_progress_callback = Mock()
    sync_finished_callback = Mock()
    synchronizer_thread.sync_started.connect(sync_started_callback)
    synchronizer_thread.load_progress.connect(load_progress_callback)
    synchronizer_thread.analyze_progress.connect(analyze_progress_callback)
    synchronizer_thread.sync_finished.connect(sync_finished_callback)

    synchronizer_thread.start()
    detect_changes()
    synchronizer_thread.join()
    hand = session.query(Hand).one()
    assert hand.analyzed is True
    assert len(session.query(Fact).all()) >= 1
    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert round(history_file.last_sync_datetime.timestamp()) == round(
        f.stat().st_mtime
    )
    assert (
        len([record for record in caplog.records if record.levelname == "ERROR"]) == 0
    )

    sync_started_callback.assert_called_once_with()
    load_progress_callback.assert_not_called()
    analyze_progress_callback.assert_not_called()
    sync_finished_callback.assert_called_once_with()


def test_synchronizer_error_while_parsing(
    session, history, raw_hand, caplog, detect_changes
):
    f = Path(history.path) / "tournament.txt"

    # Bad encoding to raise exception
    f.write_text(raw_hand("hand_1"), encoding="utf-16")

    synchronizer_thread = SynchronizerThread(history)

    sync_started_callback = Mock()
    load_progress_callback = Mock()
    sync_finished_callback = Mock()
    synchronizer_thread.sync_started.connect(sync_started_callback)
    synchronizer_thread.load_progress.connect(load_progress_callback)
    synchronizer_thread.sync_finished.connect(sync_finished_callback)

    synchronizer_thread.start()
    detect_changes()
    synchronizer_thread.join()

    history_file = session.query(HistoryFile).one()
    assert history_file.last_sync_datetime is None

    sync_started_callback.assert_called_once_with()
    assert len(load_progress_callback.mock_calls) == 2
    sync_finished_callback.assert_called_once_with()

    assert any(
        [
            record
            for record in caplog.records
            if record.levelname == "ERROR"
            and "An error occured while parsing HistoryFile" in record.message
        ]
    )


def test_synchronizer_unanalyzed_hand(
    session, history, raw_hand, caplog, detect_changes
):
    f = Path(history.path) / "tournament.txt"
    f.write_text(raw_hand("hand_1"))

    synchronizer_thread = SynchronizerThread(history)
    synchronizer_thread.start()
    detect_changes()
    synchronizer_thread.join()

    session.query(Fact).delete()
    hand = session.query(Hand).one()
    hand.analyzed = False
    session.commit()
    assert len(session.query(Fact).all()) == 0

    # Reload history
    session.expire(history)
    history = session.query(History).one()

    synchronizer_thread = SynchronizerThread(history)

    sync_started_callback = Mock()
    load_progress_callback = Mock()
    analyze_progress_callback = Mock()
    sync_finished_callback = Mock()
    synchronizer_thread.sync_started.connect(sync_started_callback)
    synchronizer_thread.load_progress.connect(load_progress_callback)
    synchronizer_thread.analyze_progress.connect(analyze_progress_callback)
    synchronizer_thread.sync_finished.connect(sync_finished_callback)

    synchronizer_thread.start()
    detect_changes()
    synchronizer_thread.join()
    session.expire(hand)
    hand = session.query(Hand).one()
    assert hand.analyzed is True
    assert len(session.query(Fact).all()) >= 1
    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert round(history_file.last_sync_datetime.timestamp()) == round(
        f.stat().st_mtime
    )
    assert (
        len([record for record in caplog.records if record.levelname == "ERROR"]) == 0
    )

    sync_started_callback.assert_called_once_with()
    load_progress_callback.assert_not_called()
    analyze_progress_calls = analyze_progress_callback.mock_calls
    assert len(analyze_progress_calls) == 2
    assert analyze_progress_calls[0] == call(0)
    assert analyze_progress_calls[1] == call(1)
    sync_finished_callback.assert_called_once_with()


def test_observer_file_created(session, history, detect_changes):
    f = Path(history.path) / "tournament.txt"

    observer_thread = ObserverThread(history)
    refreshed_callback = Mock()
    observer_thread.refreshed.connect(refreshed_callback)
    observer_thread.start()

    f.touch()
    detect_changes()
    refreshed_callback.assert_called_once_with()
    assert observer_thread.is_alive() is True

    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert history_file.history == history


def test_observer_file_updated(session, history, detect_changes):
    f = Path(history.path) / "tournament.txt"

    observer_thread = ObserverThread(history)
    refreshed_callback = Mock()
    observer_thread.refreshed.connect(refreshed_callback)
    observer_thread.start()

    f.touch()
    detect_changes()
    refreshed_callback.assert_called_once_with()
    assert observer_thread.is_alive() is True

    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert history_file.history == history

    f.touch()
    detect_changes()
    assert len(refreshed_callback.mock_calls) == 2
    assert observer_thread.is_alive() is True

    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert history_file.history == history


def test_observer_file_deleted(history, detect_changes):
    f = Path(history.path) / "tournament.txt"
    f.write_text("file exists")
    f.touch()

    observer_thread = ObserverThread(history)
    refreshed_callback = Mock()
    observer_thread.refreshed.connect(refreshed_callback)
    observer_thread.start()

    f.unlink()
    detect_changes()
    refreshed_callback.assert_not_called()
    assert observer_thread.is_alive() is True


def test_observer_dir_event(history, detect_changes):
    f = Path(history.path) / "subdirectory"

    observer_thread = ObserverThread(history)
    refreshed_callback = Mock()
    observer_thread.refreshed.connect(refreshed_callback)
    observer_thread.start()

    f.mkdir()
    detect_changes()
    refreshed_callback.assert_not_called()
    assert observer_thread.is_alive() is True


def test_observer_stop(history, detect_changes):
    f = Path(history.path) / "tournament.txt"

    observer_thread = ObserverThread(history)
    refreshed_callback = Mock()
    observer_thread.refreshed.connect(refreshed_callback)
    observer_thread.start()
    observer_thread.stop()

    f.touch()
    detect_changes()
    refreshed_callback.assert_not_called()

    observer_thread.join()
    assert observer_thread.is_alive() is False


def test_observer_load_new_hand(session, history, raw_hand, caplog, detect_changes):
    f = Path(history.path) / "tournament.txt"
    f.write_text(raw_hand("hand_1"))

    observer_thread = ObserverThread(history)
    refreshed_callback = Mock()
    hand_loaded_callback = Mock()
    observer_thread.refreshed.connect(refreshed_callback)
    observer_thread.hand_loaded.connect(hand_loaded_callback)
    observer_thread.start()

    f.touch()
    detect_changes()

    refreshed_callback.assert_called_once_with()
    hand_loaded_callback.assert_called_once()
    (hand,) = hand_loaded_callback.mock_calls[0].args
    assert hand.id == session.query(Hand).one().id

    assert hand.analyzed is True
    assert len(session.query(Fact).all()) >= 1
    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert round(history_file.last_sync_datetime.timestamp()) == round(
        f.stat().st_mtime
    )
    assert (
        len([record for record in caplog.records if record.levelname == "ERROR"]) == 0
    )


def test_observer_existing_hand(session, history, raw_hand, caplog, detect_changes):
    f = Path(history.path) / "tournament.txt"
    f.write_text(raw_hand("hand_1"))

    observer_thread = ObserverThread(history)
    refreshed_callback = Mock()
    hand_loaded_callback = Mock()
    observer_thread.refreshed.connect(refreshed_callback)
    observer_thread.hand_loaded.connect(hand_loaded_callback)
    observer_thread.start()

    f.touch()
    detect_changes()
    assert len(refreshed_callback.mock_calls) == 1
    assert len(hand_loaded_callback.mock_calls) == 1
    assert len(session.query(Hand).all()) == 1

    f.touch()
    detect_changes()
    assert len(refreshed_callback.mock_calls) == 2
    assert len(hand_loaded_callback.mock_calls) == 1
    hand = session.query(Hand).one()
    assert hand.analyzed is True
    assert len(session.query(Fact).all()) >= 1
    history_file = session.query(HistoryFile).one()
    assert history_file.path == str(f)
    assert round(history_file.last_sync_datetime.timestamp()) == round(
        f.stat().st_mtime
    )
    assert (
        len([record for record in caplog.records if record.levelname == "ERROR"]) == 0
    )


def test_observer_error_while_parsing(
    session, history, raw_hand, caplog, detect_changes
):
    f = Path(history.path) / "tournament.txt"

    # Bad encoding to raise exception
    f.write_text(raw_hand("hand_1"), encoding="utf-16")

    observer_thread = ObserverThread(history)
    observer_thread.start()

    f.touch()
    detect_changes()

    assert observer_thread.is_alive()

    history_file = session.query(HistoryFile).one()
    assert history_file.last_sync_datetime is None

    assert any(
        [
            record
            for record in caplog.records
            if record.levelname == "ERROR"
            and "An error occured while parsing HistoryFile" in record.message
        ]
    )


def test_is_history_file_unknown_room(room):
    assert is_history_file("unknown", Path("tournament.txt")) is False


def test_is_history_file_winamax(room):
    assert is_history_file(room, Path("tournament.html")) is False
    assert is_history_file(room, Path("tournament-summary.txt")) is False
    assert is_history_file(room, Path("tournament.txt")) is True
